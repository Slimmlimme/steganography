#ifndef PICTURECRYPT_H
#define PICTURECRYPT_H

#include <QDir>
#include <QFileDialog>
#include <QTextEdit>

class PictureCrypt
{
public:
    PictureCrypt();

    QImage Encrypt(const QByteArray& source, const QImage& picture, const QString& extension, QTextEdit* logger);
    QByteArray Decrypt(const QImage& picture, QTextEdit* logger, QString& extension);

    void SaveByte(QImage& picture, int coord_x, int coord_y, unsigned char byte);
    unsigned char GetByte(QColor color);
};

#endif // PICTURECRYPT_H
