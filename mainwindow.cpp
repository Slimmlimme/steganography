#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    image_(),
    path_()
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    path_ = QFileDialog::getOpenFileName(nullptr,QObject::tr("Выберите изображение"),QDir::homePath(), QObject::tr("Picture (*.png);;Все файлы (*.*)"));
    image_.load(path_);
    QPixmap pix(path_);
    ui->LogsWindow->setText(ui->LogsWindow->toPlainText() + "Opened picture\n");
    //ui->picture->setFixedSize(image_.width(), image_.height());
    ui->picture->setPixmap(pix);
}

void MainWindow::on_btn_open_file_clicked()
{
    file_path_ = QFileDialog::getOpenFileName(nullptr,QObject::tr("Выберите файл"),QDir::homePath(), QObject::tr("Archive (*.rar);;Все файлы (*.*)"));
    QFile file(file_path_);
    int dot_pos = 0;
    QString extension;
    for (int i = file_path_.length() - 1; i != 0; --i) {
        if ('.' == file_path_[i]) {
            dot_pos = i;
            break;
        }
    }
    if (0 != dot_pos) {
        for (int i = dot_pos; i < file_path_.length(); ++i) {
           extension.push_back(file_path_[i]);
        }
    }
    file.open(QIODevice::ReadOnly);
    QByteArray array(file.readAll());
    file.close();
    file_bytes_ = array;
    ui->LogsWindow->setText(ui->LogsWindow->toPlainText() + "Opened file\n");
    ui->LogsWindow->setText(ui->LogsWindow->toPlainText() + "File size: " + QString::number(array.size()) + "\n");
    result_image_ = crypt_.Encrypt(file_bytes_, image_, extension, ui->LogsWindow);
    QPixmap pixmap = QPixmap::fromImage(result_image_);
    //ui->result_picture->setFixedSize(result_image_.width(), result_image_.height());
    ui->result_picture->setPixmap(pixmap);
}

void MainWindow::on_save_clicked()
{
    result_image_.save("result.png");
}

void MainWindow::on_btn_load_encrypted_clicked()
{
    path_ = QFileDialog::getOpenFileName(nullptr,QObject::tr("Выберите изображение"),QDir::homePath(), QObject::tr("Picture (*.png);;Все файлы (*.*)"));
    encrypted_image_.load(path_);
    QPixmap pix(path_);
    //ui->result_picture->setFixedSize(encrypted_image_.width(), encrypted_image_.height());
    ui->result_picture->setPixmap(pix);
}

void MainWindow::on_btn_decrypt_clicked()
{
    QString extension;
    decrypted_file_ = crypt_.Decrypt(encrypted_image_, ui->LogsWindow, extension);
    QFile file("result" + extension);
    file.open(QIODevice::WriteOnly);
    file.write(decrypted_file_);
    file.close();
}
