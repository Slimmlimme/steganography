#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDir>
#include <QFileDialog>
#include <QImage>
#include <QLabel>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>

#include "picturecrypt.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_btn_open_file_clicked();

    void on_save_clicked();

    void on_btn_load_encrypted_clicked();

    void on_btn_decrypt_clicked();

private:
    Ui::MainWindow *ui;
    QImage image_;
    QString path_;
    QString file_path_;
    QGraphicsScene scene;
    QFile file_;
    QByteArray file_bytes_;
    PictureCrypt crypt_;
    QImage result_image_;
    QImage encrypted_image_;
    QByteArray decrypted_file_;
};

#endif // MAINWINDOW_H
