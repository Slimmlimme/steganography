#include "picturecrypt.h"

PictureCrypt::PictureCrypt()
{

}

QImage PictureCrypt::Encrypt(const QByteArray &source, const QImage &picture, const QString& extension, QTextEdit* logger)
{
    QImage result = picture;
    QColor color;
    int cur_byte = 0;
    int size = source.size();
    unsigned char extension_size = static_cast<unsigned char>(extension.length());
    unsigned char size_byte1 = static_cast<unsigned char>(size);
    unsigned char size_byte2 = static_cast<unsigned char>(size >> 8);
    unsigned char size_byte3 = static_cast<unsigned char>(size >> 16);
    unsigned char size_byte4 = static_cast<unsigned char>(size >> 24);
    unsigned char bytes[4] = {size_byte1, size_byte2, size_byte3, size_byte4};
    logger->setText(logger->toPlainText() + "S B1: " + QString::number(bytes[0]) + " B2: " + QString::number(bytes[1]) + " B3: " + QString::number(bytes[2]) + " B4: " + QString::number(bytes[3]) + "\n");
    for (int i = 0; i < result.width(); ++i) {
        for (int j = 0; j < result.height(); ++j) {
            if (j < 4 && 0 == i) {
                SaveByte(result, i, j, bytes[j]);
            } else if (4 == j && 0 == i) {
                SaveByte(result, i, j, extension_size);
            } else if (j > 4 && 0 == i) {
                SaveByte(result, i, j, static_cast<unsigned char>(extension[j - 5].toLatin1()));
            } else {
                if (source.size() > cur_byte) {
                    unsigned char byte = static_cast<unsigned char>(source[cur_byte]);
                    SaveByte(result, i, j, byte);
                    ++cur_byte;
                } else {
                    break;
                }
            }
        }
    }
    return result;
}

QByteArray PictureCrypt::Decrypt(const QImage &picture, QTextEdit* logger, QString& extension)
{
    QByteArray array;
    QColor color;
    unsigned int size = 0;
    unsigned int cur_size = 0;
    unsigned char bytes[4];
    bool is_in_size = true;
    for (int i = 0; i < 4; ++i) {
       color = QColor(picture.pixel(0, i));
       bytes[i] = GetByte(color);
    }
    size = size | bytes[0];
    size = size | static_cast<unsigned int>((bytes[1] << 8));
    size = size | static_cast<unsigned int>((bytes[2] << 16));
    size = size | static_cast<unsigned int>((bytes[3] << 24));
    logger->setText(logger->toPlainText() + "Size: " + QString::number(size) + "\n");

    color = QColor(picture.pixel(0, 4));
    unsigned char extension_size = GetByte(color);
    for (int i = 5; i < 5 + extension_size; ++i) {
        color = QColor(picture.pixel(0, i));
        extension.push_back(GetByte(color));
    }
    for (int i = 0; i < picture.width() && is_in_size; ++i) {
        for (int j = 0; j < picture.height() && is_in_size; ++j) {
           color = QColor(picture.pixel(i,j));
           do {
               if (j < 5 + extension_size && i == 0) {
                   break;
               }

               array.push_back(static_cast<char>(GetByte(color)));
               ++cur_size;
               if (cur_size == size) {
                   is_in_size = false;
               }
           } while (false);
        }
    }

    return array;
}

void PictureCrypt::SaveByte(QImage &picture, int coord_x, int coord_y, unsigned char byte)
{
    QRgb pixel;
    QColor color;
    pixel = picture.pixel(coord_x,coord_y);
    color = QColor(pixel);
    unsigned char bits1 = byte >> 4;
    unsigned char bits2 = byte >> 2;
    bits2 = static_cast<unsigned char>(bits2 << 6);
    bits2 = bits2 >> 6;
    unsigned char bits3 = static_cast<unsigned char>(byte << 6);
    bits3 = bits3 >> 6;
    int red = color.red();
    int green = color.green();
    int blue = color.blue();
    red = red >> 4;
    red = red << 4;
    green = green >> 2;
    green = green << 2;
    blue = blue >> 2;
    blue = blue << 2;
    red = red | bits1;
    green = green | bits2;
    blue = blue | bits3;
    //logger->setText(logger->toPlainText() + "R: " + QString::number(red) + " G: " + QString::number(green) + " B: " + QString::number(blue) + "\n");
    color.setRed(red);
    color.setBlue(blue);
    color.setGreen(green);
    color.getRgb(&red, &green, &blue);
    picture.setPixelColor(coord_x,coord_y, color);
}

unsigned char PictureCrypt::GetByte(QColor color)
{
    unsigned char result = 0;
    unsigned char red = static_cast<unsigned char>(color.red());
    unsigned char green = static_cast<unsigned char>(color.green());
    unsigned char blue = static_cast<unsigned char>(color.blue());
    red = static_cast<unsigned char>(red << 4);
    green = static_cast<unsigned char>(green << 6);
    green = green >> 4;
    blue = static_cast<unsigned char>(blue << 6);
    blue = blue >> 6;
    result = result | red;
    result = result | green;
    result = result | blue;
    return result;
}
